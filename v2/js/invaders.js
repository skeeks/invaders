/*
 * 
 * All velocitys are measured in px per 100ms
 */


$(document).ready(function(){
    Invaders.images = { ship: 'ship.png', 
                        enemy: 'enemy.png', 
                        enemy_shell: 'enemy_shell.png',
                        ship_shell: 'ship_shell.png',
                        final_score: 'final_score.png'
                    };
    Invaders.init(function (){
        var ship = new Ship({x:100});
        Invaders.setNewShip(ship);
        Invaders.stage.draw();
    }); 
    $("#button").on('click.start', function(){
        Invaders.requestInvaderFrame();
        $(this).remove();
    });
});

/* INVADERS ::::::::::::::::::::::::::::::::::::::::::
* Copyright by SixKnight 2013
*/

var Invaders = {
 
/*
 * Used object attributes in Invaders:
 * stage        - The canvas object wrapped in a kinetic.js stage
 * ship         - The player's ship wrapped in a kineticjs shape
 * shipLayer    - The layer of the ship wrapped in a kinetcjs layer
 * shellLayer   - The layer of the shells wrapped in a kinetcjs layer
 * enemyLayer   - The layer of the enemys wrapped in a kinetcjs layer
 * score        - The current score scored by the player
 * lastDrawTime - timestamp of last draw
 * enemys       - Array of Enemy Objects
 * shipShells   - Array of Shell Objects, shot from player ship
 * enemyShells  - Array of Shell Objects, shot from the enemy ships
 * gameover     - Boolean, indicates whether the game is over or not
 * paused       - Boolean, indicates whether the game is paused or not
 * lastEnemySpawn   - timestamp when the last enemy spawned 
 */
/*:::::::::::::::::*/
    init: function init(callback) {
        this.loadImages(function (){
            this.stage = new Kinetic.Stage({
                container: 'gamePane',
                width: settings.PANE_WIDTH,
                height: settings.PANE_HEIGHT
            });
            this.initLayers();
            this.stage.draw();
            callback();
        });
        this.ship;
        this.score = 0;
        this.lastDrawTime;
        this.enemys = [];
        this.shipShells = [];
        this.enemyShells = [];
        this.gameover = false;
        this.paused = false;
        this.lastEnemySpawn;
    }
/*:::::::::::::::::*/
    , initLayers: function createLayers(){
        this.shipLayer = new Kinetic.Layer();
        this.enemyLayer = new Kinetic.Layer();
        this.shellLayer = new Kinetic.Layer();
        
        this.stage.add(this.shellLayer);
        this.stage.add(this.enemyLayer);
        this.stage.add(this.shipLayer);
    }
/*:::::::::::::::::*/
    , addShipShell: function addShipShell(shell) {
        this.shipShells.push(shell);
        this.shellLayer.add(shell.kinetic);
    }
/*:::::::::::::::::*/
    , addEnemyShell: function addEnemyShell(shell) {
        this.enemyShells.push(shell);
        this.shellLayer.add(shell.kinetic);
    }
/*:::::::::::::::::*/
    , setNewShip: function setNewShip(ship){
        this.ship = ship;
        this.shipLayer.destroyChildren();
        this.shipLayer.add(this.ship.kinetic);
    }
/*:::::::::::::::::*/
    , shotShell: function shotShell(){
        this.ship.shot();
    }
/*:::::::::::::::::*/
    , enemyDestroyed: function enemyDestroyed(enemy){
        if(!enemy instanceof Enemy){
            throw new Error("Argument enemy for function enemyDestroy should be a instance of Enemy");
        }
        enemy.kinetic.remove();
        this.scoreDestroy();
        var newarr = [];
        for(var n = 0; n < this.enemys.length; n++){
            if(this.enemys[n] === enemy){
                delete this.enemys[n];
            } else {
                newarr.push(this.enemys[n]);
            }
        }
        this.enemys = newarr;
        
    }
/*:::::::::::::::::*/
    , shellDestroyed: function shellDestroyed(shell, isShipShell){
        if(!shell instanceof Shell){
            throw new Error("Argument enemy for function enemyDestroy should be a instance of Enemy");
        }
        shell.kinetic.remove();
        if(isShipShell){
            var newarr = [];
            for(var n = 0; n < this.shipShells.length; n++){
                if(this.shipShells[n] === shell){
                    delete this.shipShells[n];
                } else {
                    newarr.push(this.shipShells[n]);
                }
            }
            this.shipShells = newarr;
        } else {
            var newarr = [];
            for(var n = 0; n < this.enemyShells.length; n++){
                if(this.enemyShells[n] === shell){
                    delete this.enemyShells[n];
                } else {
                    newarr.push(this.enemyShells[n]);
                }
            }
            this.enemyShells = newarr;
        }
        
    }
/*:::::::::::::::::*/
    , shipDestroyed: function shipDestroyed(ship){
        this.gameover = true;
        this.terminate();
    }
/*:::::::::::::::::*/
    , spawnEnemy: function spawnEnemy(){
        var y = rand(0, settings.PANE_HEIGHT - 40);
        var enemy = new Enemy({
            x: parseInt(settings.PANE_WIDTH - 40 ,10),
            y: parseInt(y, 10),
            velocity: {
                x: settings.ENEMY_VELOCITY_X,
                y: settings.ENEMY_VELOCITY_Y
            }
        });
        this.enemys.push(enemy);
        this.enemyLayer.add(enemy.kinetic);
    }
/*:::::::::::::::::*/
    , enemyController: function enemyController(){
        var timestamp = (new Date()).getTime();
        if(isNaN(this.lastEnemySpawn)){
            this.lastEnemySpawn = timestamp;
        }
        if(timestamp - this.lastEnemySpawn >= settings.ENEMY_SPAWN_INTERVAL){
            this.spawnEnemy();
            this.lastEnemySpawn = timestamp;
        }
    }
/*:::::::::::::::::*/
    , reckonEnemyPositions: function reckonEnemyPositions(elapsedTime){
        var timesElapsed = elapsedTime / 100;
        var enemys = Kinetic.Collection.toCollection(this.enemys);
        enemys.each(function(enemy, n){
            enemy.reckonPosition(timesElapsed);
        });
    }
/*:::::::::::::::::*/
    , reckonEnemyShots: function reckonEnemyShots(){
        var enemys = Kinetic.Collection.toCollection(this.enemys);
        enemys.each(function(enemy, n){
            enemy.shot();
        });
        
    }
/*:::::::::::::::::*/
    , reckonShellPositions: function reckonShellPositions(elapsedTime) {
        var timesElapsed = elapsedTime / 100;
        var sshells = Kinetic.Collection.toCollection(this.shipShells);
        sshells.each(function(sshell, n){
            sshell.reckonPosition(timesElapsed);
        });
        var eshells = Kinetic.Collection.toCollection(this.enemyShells);
        eshells.each(function(eshell, n){
            eshell.reckonPosition(timesElapsed);
        });
    }
/*:::::::::::::::::*/
    , reckonShipPosition: function reckonShipPosition(elapsedTime){
        this.ship.reckonPosition(elapsedTime);
    }
/*:::::::::::::::::*/
    , reckonShipShellDamage: function reckonShipShellDamage(){
        var that = this;
        var shells = Kinetic.Collection.toCollection(this.shipShells);
        shells.each(function(shell, n){
            if(shell.inTheGame){
                var beginX = shell.kinetic.getX();
                var beginY = shell.kinetic.getY();
                var endY = beginY + shell.kinetic.getHeight();
                var endX = beginX + shell.kinetic.getWidth();
                var enemys = Kinetic.Collection.toCollection(that.enemys);
                var shellarea = new Area(beginX, beginY, endX, endY);
                enemys.each(function(enemy, i){
                    var enemy_area = new Area(enemy.kinetic.getX(),
                                            enemy.kinetic.getY(), 
                                            enemy.kinetic.getX() + enemy.kinetic.getWidth(), 
                                            enemy.kinetic.getY() + enemy.kinetic.getHeight()
                                    );
                    if(enemy_area.overlaps(shellarea)){
                        that.scoreHit();
                        enemy.damage(shell.damage, true);
                        shell.destroy();
                    }
                });
            }
        });
    }
/*:::::::::::::::::*/
    , scoreHit: function(){
        this.score += settings.SCORE_PER_HIT;
    }
/*:::::::::::::::::*/
    , scoreDestroy: function(){
        this.score += settings.SCORE_PER_DESTROY;
    }
/*:::::::::::::::::*/
    , updateScore: function(){
        $("#score").text(this.score);
    }
/*:::::::::::::::::*/
    , drawFinalScore: function(){
        var imageFS = new Image();
        imageFS.src = settings.IMAGE_FOLDER + settings.FINAL_SCORE_IMAGE;
        var finalScore = new Kinetic.Image({
            image: imageFS,
            x: 0,//this.stage.getWidth() / 2 - imageFS.width,
            y: 0,//this.stage.getHeight() / 2 - imageFS.height,
            width: imageFS.width,
            height: imageFS.height
        });
        var scoreText = new Kinetic.Text({
            text: 'Your score: ' + this.score,
            x: 0,
            y: 50,
            fontFamily: 'Arial Black',
            fontSize: 25,
            align: 'center',
            fill: '#000000',
            width: imageFS.width,
            height: 25
        });
        var fsLay = new Kinetic.Layer({
            x: this.stage.getWidth() / 2 - imageFS.width,
            y: this.stage.getHeight() / 2 - imageFS.height,
            width: imageFS.width,
            height: imageFS.height
        });
        fsLay.add(finalScore);
        fsLay.add(scoreText);
        this.stage.add(fsLay);
    }
/*:::::::::::::::::*/
    , reckonEnemyShellDamage: function reckonEnemyShellDamage(){
        var that = this;
        var shells = Kinetic.Collection.toCollection(this.enemyShells);
        var ship_area = new Area(this.ship.kinetic.getX(),
                                this.ship.kinetic.getY(), 
                                this.ship.kinetic.getX() + this.ship.kinetic.getWidth(), 
                                this.ship.kinetic.getY() + this.ship.kinetic.getHeight()
                        );
        shells.each(function(shell, n){
            var beginX = shell.kinetic.getX();
            var beginY = shell.kinetic.getY();
            var endY = beginY + shell.kinetic.getHeight();
            var endX = beginX + shell.kinetic.getWidth();
            var area = new Area(beginX, beginY, endX, endY);
            if(ship_area.overlaps(area)){
                that.ship.damage(shell.damage, false);
                shell.destroy();
            }
        });
    }
/*:::::::::::::::::*/
    , pause: function pause(){
        this.paused = true;
    }
/*:::::::::::::::::*/
    , resume: function resume(){
        this.paused = false;
        this.lastDrawTime = NaN;
        this.requestInvaderFrame();
    }
/*:::::::::::::::::*/
    , terminate: function terminate(){
        this.gameover = true;
        this.drawFinalScore();
        Invaders = {};
    }
/*:::::::::::::::::*/
    , draw: function draw(timestamp){    
        var elapsedTime = 0;
        if(!isNaN(this.lastDrawTime)){
            elapsedTime = timestamp - this.lastDrawTime;
        }
        if(UserEvents.shotBtn){
            this.shotShell();
        }
        if(UserEvents.shipDownBtn || UserEvents.shipUpBtn || UserEvents.shipForwardBtn || UserEvents.shipBackwardBtn){
            this.reckonShipPosition(elapsedTime);
        }
        this.enemyController();
        /*
         * Calculate the new positions of the shells
         */
        this.reckonShellPositions(elapsedTime);
        this.reckonShipShellDamage();
        this.reckonEnemyPositions(elapsedTime);
        this.reckonEnemyShots();
        this.reckonEnemyShellDamage();
        this.updateScore();
        // Update the lastDrawTime object attribute
        this.lastDrawTime = timestamp;
        // Draw stage 
        this.stage.draw();
        // Request next animation frame
        if(!this.gameover && !this.paused){
            this.requestInvaderFrame();
        }
    }
/*:::::::::::::::::*/ 
    , requestInvaderFrame: function(){
        requestAnimFrame(function(timestamp){
            Invaders.draw.call(Invaders, timestamp);
        });
    }
/* HELPER FUNCTIONS */
/*:::::::::::::::::*/
    , loadImages: function loadImages(callback){
        var that = this;
        var images = this.images;
        var imagesLoaded = 0;
        function imageLoaded(){
            imagesLoaded++;
            if(imagesLoaded === Object.keys(images).length) {
                callback.call(Invaders); 
            }
        }
        var imageTemp = [];
        var i = 0;
        for(var n in that.images){
            imageTemp[i] = document.createElement('img');
            imageTemp[i].onload = function(){
                imageLoaded();
                
            };
            imageTemp[i].src = 'images/' + that.images[n];
            that.images[n] = imageTemp[i];
            i++;
        }
    }
};

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(function(){ callback(1000/60)}, 1000 / 60);
          };
})();

function rand(min, max){
    var argc = arguments.length;
    if (argc === 0) {
        min = 0;
        max = 2147483647;
    } else if (argc === 1) {
        throw new Error('Warning: rand() expects exactly 2 parameters, 1 given');
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var log = console.log.bind(console);