UserEvents = {
    shotBtn: false,
    shipDownBtn: false,
    shipUpBtn: false,
    shipForwardBtn: false,
    shipBackwardBtn: false
};

$(document).ready(function(){
   $(document).on('keydown', function(evt){
       var keyCode = evt.which;
       if(keyCode === 32){  // Enter hold
           UserEvents.shotBtn = true;
       }
       if(keyCode === 83){  // S hold
           UserEvents.shipDownBtn = true;
       }
       if(keyCode === 87){  // W hold
           UserEvents.shipUpBtn = true;
       }
       if(keyCode === 68){  // D hold
           UserEvents.shipForwardBtn = true;
       }
       if(keyCode === 65){  // A Hold
           UserEvents.shipBackwardBtn = true;
       }
   }); 
   $(document).on('keyup', function(evt){
       var keyCode = evt.which;
       if(keyCode === 32){  // Enter releas
           UserEvents.shotBtn = false;
       }
       if(keyCode === 83){  // S release
           UserEvents.shipDownBtn = false;
       }
       if(keyCode === 87){  // W release
           UserEvents.shipUpBtn = false;
       }
       if(keyCode === 68){  // D release
           UserEvents.shipForwardBtn = false;
       }
       if(keyCode === 65){  // A release
           UserEvents.shipBackwardBtn = false;
       }
   }); 
   $(window).on('blur', function(evt){
        UserEvents.shotBtn = false;
        UserEvents.shipDownBtn = false;
        UserEvents.shipUpBtn = false;
        UserEvents.shipForwardBtn = false;
        UserEvents.shipBackwardBtn = false;
   });
});