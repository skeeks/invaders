
/*
 * Used object attributes for object Ship:
 * image    - the image object of the Ship
 * kinetic  - Kinetic Shape of the Ship
 * velocity - Velocity Object. Values: x and y
 * lastShot - timestamp of the last shot time
 * health   - the health points of the ship
 */
function Ship(parameters){
    parameters = parameters || {};
    this.image = new Image();
    this.kinetic = null;
    this.velocity = {};
    this.health = 0;
    this.lastShot = 0;
    this._construct(parameters);
}

Ship.prototype._construct = function Ship_construct(parameters){
    this.image.src = settings.IMAGE_FOLDER + (parameters.image || settings.SHIP_IMAGE);
    this.velocity = parameters.velocity || {x: settings.SHIP_VELOCITY_X, y: settings.SHIP_VELOCITY_Y};
    this.health = parameters.health || settings.SHIP_HEALTH;
    this.kinetic = new Kinetic.Image({
        x: parameters.x || 0,
        y: parameters.y || 0,
        image: this.image,
        width: this.image.width,
        height: this.image.height
    });
};

Ship.prototype.reckonXPosition = function Ship_reckonXPosition(elapsedTime){
    var deltaX = 0;
    if(UserEvents.shipForwardBtn){
        deltaX += parseInt(this.velocity.x * (elapsedTime / 100), 10);
    } 
    if(UserEvents.shipBackwardBtn){
        deltaX -= parseInt(this.velocity.x * (elapsedTime / 100), 10);
    }
    this.kinetic.move(deltaX, 0);
    if(this.isOutOfPane()){
        this.kinetic.move(0-deltaX, 0);
    }
};

Ship.prototype.reckonYPosition = function Ship_reckonYPosition(elapsedTime){
    var deltaY = 0;
    if(UserEvents.shipDownBtn){
        deltaY += parseInt(this.velocity.y * (elapsedTime / 100), 10);
    }
    if(UserEvents.shipUpBtn){
        deltaY -= parseInt(this.velocity.y * (elapsedTime / 100), 10);
    }
    this.kinetic.move(0, deltaY);
    if(this.isOutOfPane()){
        this.kinetic.move(0, 0-deltaY);
    }
};

Ship.prototype.reckonPosition = function Ship_reckonPosition(elapsedTime){
    if(UserEvents.shipDownBtn || UserEvents.shipUpBtn){
        this.reckonYPosition(elapsedTime);
    }
    if(UserEvents.shipBackwardBtn || UserEvents.shipForwardBtn){
        this.reckonXPosition(elapsedTime);
    }
};

Ship.prototype.shot = function Ship_shot(){
    var timestamp = (new Date()).getTime();
    if(parseInt(this.lastShot + settings.SHIP_SHOT_INTERVAL, 10) <= timestamp){
        var x = this.kinetic.getX() + this.kinetic.getWidth() / 2;
        var y = this.kinetic.getY() + this.kinetic.getHeight() / 2;
        var shell = new Shell({
            image: settings.SHIP_SHELL_IMAGE,
            x: x, 
            y: y, 
            velocity: settings.SHELL_VELOCITY,
            damage: settings.SHIP_SHELL_DAMAGE
        });
        Invaders.addShipShell.call(Invaders, shell);
        this.lastShot = timestamp;
    }
};

Ship.prototype.damage = function Ship_damage(damage){
    this.health -= damage;
    this.checkHealth();
};

Ship.prototype.heal = function Ship_heal(heal){
    this.health += heal;
    this.checkHealth();
};

Ship.prototype.checkHealth = function Ship_checkHealth(){
    if(this.health <= 0){
        Invaders.shipDestroyed(this);
    }
};

Ship.prototype.isOutOfPane = function Ship_isOutOfPane(){
    var kobj = this.kinetic;
    if(     kobj.getX() >= 0 
            && kobj.getY() >= 0 
            && kobj.getX() + kobj.getWidth() < settings.PANE_WIDTH 
            && kobj.getY() + kobj.getHeight() < settings.PANE_HEIGHT )
    {
        return false;
    } else {
        return true;
    }
};
/*
 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 */
/*
 * Used object attributes in object Shell:
 * damage   - The amount of damage which this Shell does
 * image    - Image Object of the image 
 * kinetic  - Kinetic Shape of the Shell
 * velocity - The x velocity speed
 * inTheGame- Indicates whether the shell is already destroyed but not yet removed from the array which is looping
 */
function Shell(parameters) {
    parameters = parameters || {};
    this.damage = 0;
    this.image = new Image();
    this.kinetic = null;
    this.velocity = 0;
    this._construct(parameters);
    // This boolean is necessary, else the game will damage a ship more than one time when a shell hit the ship
    this.inTheGame = true; 
}

Shell.prototype._construct = function Shell_construct(parameters){
    this.image.src = settings.IMAGE_FOLDER + (parameters.image || settings.SHELL_IMAGE);
    var that = this;
    this.velocity = parameters.velocity || settings.SHELL_VELOCITY;
    this.damage = parameters.damage || settings.SHELL_DAMAGE;
    this.kinetic = new Kinetic.Image({
        x: parameters.x || 0,
        y: parameters.y || 0,
        image: that.image,
        width: parseInt(that.image.width, 10),
        height: parseInt(that.image.height, 10)
    });
};

Shell.prototype.destroy = function Shell_destroy(isShipShell){
    this.inTheGame = false;
    Invaders.shellDestroyed(this, isShipShell);
};

Shell.prototype.reckonPosition = function Shell_reckonPosition(timesElapsed){
    if(this.inTheGame){
        var deltaX = parseInt(this.velocity * timesElapsed, 10);
        this.kinetic.move(deltaX, 0);
        if(this.isOutOfPane()){
            this.destroy(true);
            this.destroy(false);
        }
    }
};

Shell.prototype.isOutOfPane = function Shell_isOutOfPane(){
    var kobj = this.kinetic;
    if(     kobj.getX() >= 0 
            && kobj.getY() >= 0 
            && kobj.getX() + kobj.getWidth() < settings.PANE_WIDTH 
            && kobj.getY() + kobj.getHeight() < settings.PANE_HEIGHT )
    {
        return false;
    } else {
        return true;
    }  
};

/*
 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 */
/*
 * Used object attributes for object Enemy:
 * image    - the image object of the Enemy
 * kinetic  - Kinetic Shape of the Enemy
 * velocity - Velocity Object. Values: x and y
 * lastShot - timestamp of the last shot time
 * health   - the health points of the Enemy
 */
function Enemy(parameters){
    parameters = parameters || {};
    this.image = new Image();
    this.kinetic= null;
    this.velocity = {x: 0, y: 0};
    this.lastShot = 0;
    this.health = 0;
    this._construct(parameters);
}

Enemy.prototype._construct = function Enemy_construct(parameters){
    this.image.src = settings.IMAGE_FOLDER + (parameters.image || settings.ENEMY_IMAGE);
    this.velocity.x = parameters.velocity.x || settings.ENEMY_VELOCITY_X;
    this.velocity.y = parameters.velocity.y || settings.ENEMY_VELOCITY_Y;
    this.health = parameters.health || settings.ENEMY_HEALTH;
    this.kinetic = new Kinetic.Image({
        x: parameters.x || 0,
        y: parameters.y || 0,
        image: this.image,
        width: this.image.width,
        height: this.image.height
    });
};

Enemy.prototype.shot = function Enemy_shot(){
    var timestamp = (new Date()).getTime();
    if(parseInt(this.lastShot + settings.ENEMY_SHOT_INTERVAL, 10) <= timestamp){
        var x = this.kinetic.getX() + this.kinetic.getWidth() / 2;
        var y = this.kinetic.getY() + this.kinetic.getHeight() / 2;
        var shell = new Shell({
            image: settings.ENEMY_SHELL_IMAGE,
            x: x, 
            y: y, 
            velocity: 0-settings.SHELL_VELOCITY,
            damage: settings.ENEMY_SHELL_DAMAGE
        });
        Invaders.addEnemyShell.call(Invaders, shell);
        this.lastShot = timestamp;
    }
};

Enemy.prototype.reckonPosition = function reckonPosition(timesElapsed){
    var deltaX = parseInt(this.velocity.x * timesElapsed, 10);
    var deltaY = parseInt(this.velocity.y * timesElapsed, 10);
    this.kinetic.move(0 - deltaX, deltaY);
    if(this.isOutOfPane()){
        this.destroy();
    }
};

Enemy.prototype.destroy = function Enemy_destroy(){
    Invaders.enemyDestroyed(this);
};

Enemy.prototype.damage = function Enemy_damage(damage){
    this.health -= damage;
    this.checkHealth();
};

Enemy.prototype.checkHealth = function Enemy_checkHealth(){
    if(this.health <= 0){
        Invaders.enemyDestroyed(this);
    }
};

Enemy.prototype.isOutOfPane = function isOutOfPane(){
    var kobj = this.kinetic;
    if(     kobj.getX() >= 0 
            && kobj.getY() >= 0 
            && kobj.getX() + kobj.getWidth() < settings.PANE_WIDTH 
            && kobj.getY() + kobj.getHeight() < settings.PANE_HEIGHT )
    {
        return false;
    } else {
        if(kobj.getX() <= 0){
            Invaders.enemyIrrupted(this);
        }
        return true;
    }  
};

/*
 * NOTICE FOR AREA OBJECT
 * If you call the function overlaps and the smaller area is the calling object, it will not
 * return the right value.
 * Swap the object so that ALWAYS the bigger area is the calling object and the SMALLER is passed as ARGUMENT
 */

/*
 * Used object attributes for object Area:
 * x1   - X-Coordinate of the left top corner point
 * y1   - Y-Coordinate of the left top corner point
 * x2   - X-Coordinate of the right bottom corner point
 * y2   - Y-Coordinate of the right bottom corner point
 */
function Area(x1, y1, x2, y2){
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
}

Area.prototype.isInside = function Area_isInside(x, y){
    var xInside = false;
    var yInside = false;
    if(x >= this.x1 && x <= this.x2){
        xInside = true;
    }
    if(y >= this.y1 && y <= this.y2){
        yInside = true;
    }
    return xInside && yInside;
};

Area.prototype.overlaps = function Area_overlaps(area){
  if(!area instanceof Area){
      throw new Error("Argument area for function Area_overlaps should be a instance of Area");
  }  
  
  return this.isInside(area.x1, area.y1) || this.isInside(area.x2, area.y2) || this.isInside(area.x2, area.y1) || this.isInside(area.x1, area.y2);
  
};

function rand(min, max){
    var argc = arguments.length;
    if (argc === 0) {
        min = 0;
        max = 2147483647;
    } else if (argc === 1) {
        throw new Error('Warning: rand() expects exactly 2 parameters, 1 given');
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var log = console.log.bind(console);