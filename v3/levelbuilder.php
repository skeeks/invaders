<!DOCTYPE html>
<html>
    <head>
        <title>Build your own level</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <script type="text/javascript" src="js/jquery.js"></script>
        <style>
            @import url(http://fonts.googleapis.com/css?family=Revalia);
            html, body {
                padding: 0;
                margin: 0;
                width: 100%;
                height: 100%;
            }
            body {
                background: url(images/starfield.png) repeat left top;;
            }
            #content {
                margin: 0px auto;
                width: 850px;
                padding: 16px;
                color: white;
                font-size: 20px;
                background-color: #0E0E0E;
                font-family: 'Revalia', cursive;
                border: 1px solid grey;
            }
            
            input[type="text"], input[type="password"], select {
                background-color: white;
                display: inline-block;
                padding: 4px 6px;
                border: 1px solid black;
                margin: 2px 4px;
            }
            #formList {
                list-style-type: none;
                width: 800px;
                padding: 0;
                margin: 0;
            }
            #formList li {
                margin: 0;
                padding: 0;
                border-top: 2px solid white;
            }
            #formList span {
                display: inline-block;
                min-width: 350px;
                padding: 20px 0px;
                margin: 2px 6px;
            }
        </style>
            
    </head>
    <body>
        <div id="content">
            <h1>Invaders levelbuilder</h1>
            
            <ul id="formList">
                <li>
                    <span>Points per enemy hit</span>
                    <span>
                        <input type="range" id="SPH_range" value="50" min="0" max="500"/>
                        <input type="text" id="SPH" value="50" name="SCORE_PER_HIT" size="10">
                    </span>
                </li>
                <li>
                    <span>Points per destroyed enemy</span>
                    <span>
                        <input type="range" id="SPD_range" value="50" min="0" max="500" />
                        <input type="text" id="SPD" value="50" name="SCORE_PER_DESTROY" size="10" />
                    </span>
                </li>
                <li>
                    <span>Ship shot Interval <br>milliseconds</span>
                    <span>
                        <input type="range" id="SSI_range" value="50" min="0" max="2000" />
                        <input type="text" id="SSI" value="50" name="SHIP_SHOT_INTERVAL" size="10" />
                    </span>
                </li>
                <li>
                    <span>Points per destroyed enemy</span>
                    <span>
                        <input type="range" id="SPD_range" value="50" min="0" max="500" />
                        <input type="text" id="SPD" value="50" name="SCORE_PER_DESTROY" size="10" />
                    </span>
                </li>
                <li>
                    <span>Points per destroyed enemy</span>
                    <span>
                        <input type="range" id="SPD_range" value="50" min="0" max="500" />
                        <input type="text" id="SPD" value="50" name="SCORE_PER_DESTROY" />
                    </span>
                </li>
            </ul>
        </div>
        <script>
        $(document).ready(function(){
            $("#SPH_range").on('change', function(){
                $('#SPH').val(this.value);
            });
            $("#SPD_range").on('change', function(){
                $('#SPD').val(this.value);
            });
            $("#SSI_range").on('change', function(){
                $('#SSI').val(this.value);
            });
        });
        </script>
    </body>
</html>
